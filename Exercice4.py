from helpers import arduino_simulator, MathHelper
import requests

'''
Exercice 4: (11 points)
Cet exercice comporte trois parties:
    partie 1:
        Ecrire un programme qui calcule la mediane et la moyenne chaque chaque fois que simulateReadAnalogPin() renvoie 40 valeurs.

        Ces deux informations sont ensuite immédiatement envoyées à votre serveur Flask sur IBM Cloud (crée en cours) à cette url:
        https://dns_serveur/api/addValues?mean=xxx&median=xxx&id_capteur=xxx

        id_capteur est un identifiant unique de capteur qui sera saisi par l'utilisateur au démarrage du programme. 

        Le programme tourne à l'infini
    
    Partie 2:
        Implémenter dans le serveur flask le verbe addValues?mean=xxx&median=xxx&id_capteur=xxx
        Les paramètres peuvent être stockés en GET ou en POST
        Les différentes valeurs doivent être stockées par le serveur
        Le stockage peut se faire en RAM (attention à la consommation mémoire dans ce cas).
        Le stockage BDD est optionnel mais peut être fait en bonus.

    Partie 3:
    Ajouter une page web au serveur Flask qui propose une interface agréble pour visualiser les différentes mesures renvoyées par les capteurs.
    La page doit afficher ces informations pour chaque capteur:
    - Son identifiant
    - Les 5 dernières valeurs reçues
    - Une ou pusieurs visualisations graphiques. Par exemple: 
        - Courbe des données à travers le temps
        - Histogramme
        - ou autre
    La page doit se mettre à jour automatiquement toutes les 10 secondes 
    (indice1: vous pouvez créer un verbe qui liste les capteurs et leurs 5 dernières valeurs)
    (indice2: Côté Javascript, vous pouvez utiliser une librairie ou la fonction fetch pour appeler les api)
    L'utl de la page sera: https://dns_serveur/iot_status.html

'''

nbVals = 40
vals = []
BASE_URL = "https://api-python-poc.eu-gb.mybluemix.net/"

sensor_id = input("Veuillez saisir l'id du capteur\n")
while True:
    # On récupère la réponse du simulateur, sous forme de tuple (date, valeur)
    response = arduino_simulator.simulateReadAnalogPin()
    vals.append(response[1])
    if len(vals) == nbVals:
        mean = MathHelper.getMean(vals)
        mediane = MathHelper.getMediane(vals)
        # On envoie en post
        data = {
            "id": sensor_id,
            "date": str(response[0]),
            "moyenne": mean,
            "mediane": mediane
        }
        headers = {"Content-Type": "application/json"}
        r = requests.post((BASE_URL + "/api/addvalue"), headers=headers, json=data)
        print(r.status_code, r.reason)
        vals.clear()
