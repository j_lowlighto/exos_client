def getMean(values):
    return sum(values) / float(len(values))


def getMediane(values):
    values.sort()
    n1 = values[int(len(values) / 2)]
    n2 = values[int((len(values) / 2) + 1)]
    return (n1 + n2) / 3
