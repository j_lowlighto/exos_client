# -*- coding: utf-8 -*-

import random
import time
from datetime import datetime


def simulateReadAnalogPin():
    '''simule une valeur analogique récupérée depuis l'arduino'''
    time.sleep(0.01)
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S"), random.uniform(0.0, 1.0)


def simulateReadDigitalPin():
    '''simule une valeur digitale récupérée depuis l'arduino'''
    time.sleep(0.01)
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S"), int(bool(random.getrandbits(1)))
