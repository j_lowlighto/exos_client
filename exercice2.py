import sys

import time
from writer.CustomWriterJSON import CustomWriterJSON
from helpers import MathHelper, arduino_simulator

''' 
Exercice 2: 3 points
Ecrire un programme qui reprend exercice_exemple.py mais qui les résultats affiche dans la console au format json.
Les données à afficher:
- date: au format
- mean: la moyenne des 40 dernières valeurs lues
- meadian: la medianne des 40 dernières valeurs lues

exemple:
[
    {
        date:'2018-11-22T13:17:27.914726',
        mean:0.4
        median:0.5
    },
    {
        date:'2018-11-22T13:17:27.914726',
        mean:0.1
        median:0.15
    }
]
'''

nbVals = 40
time_limit = 5
start = time.time()
vals = []

# On instancie un CustomWriterJSON
writer = CustomWriterJSON(target_file=sys.stdout)

# Tant que le temps n'est pas écoulé
while time.time() - start < time_limit:
    # On récupère la réponse du simulateur, sous forme de tuple (date, valeur)
    response = arduino_simulator.simulateReadAnalogPin()
    vals.append(response[1])
    if len(vals) == nbVals:
        mean = MathHelper.getMean(vals)
        mediane = MathHelper.getMediane(vals)
        writer.write_json({"Date": response[0], "Moyenne": mean, "Mediane": mediane},
                          on_error=lambda: print("An error occurred writing file."),
                          on_success=lambda: print("Success"))
        vals.clear()
