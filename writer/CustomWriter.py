from abc import ABCMeta


class CustomWriter:
    """
        Une classe abstraite, on hérite de cette classe pour implémenter
        un writer json ou csv
    """
    __metaclass__ = ABCMeta
    fieldnames = []
