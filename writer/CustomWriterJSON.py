import _io
import json

from writer.CustomWriter import CustomWriter


class CustomWriterJSON(CustomWriter):

    def __init__(self, target_file):
        self.target_file = target_file

    def write_json(self, data, on_error, on_success):
        """
        Ecrit un json dans le fichier
        :param on_error: on_error_occured: la fonction qui sera executée en cas d'erreur
        :param data: le contenu json a écrire
        :param on_success: la fonction a exécuter en cas de succès
        :return:
        """
        if type(self.target_file) is _io.TextIOWrapper:
            json.dump(data, self.target_file, indent=4)
        else:
            try:
                with open(self.target_file, 'w') as outfile:
                    json.dump(data, outfile, indent=4)
                    on_success()

            except:
                on_error()
