from writer.CustomWriter import CustomWriter
import csv


class CustomWriterCSV(CustomWriter):
    def __init__(self, fieldnames, delimiter, targetfile):
        self.fieldnames = fieldnames
        self.writer = csv.DictWriter(targetfile, delimiter=delimiter, quotechar='"', quoting=csv.QUOTE_MINIMAL,
                                     fieldnames=fieldnames)

    def writeheader(self):
        """
        Permet d'écrire la première ligne du fichier,
        avec les titres des colonnes.
        :return:
        """
        self.writer.writeheader()


    def writerow(self, row):
        """
        Ecrit une ligne dans le fichier, prend un dictionnaire avec en clé le fieldname,
        et la valeur.
        :param row: Dict
        :return:
        """
        self.writer.writerow(row)
