import time

from helpers import MathHelper, arduino_simulator
from writer.CustomWriterJSON import CustomWriterJSON

''' 
Exercice 3:  3 points

Ecrire un programme reprend exercice2 mais qui enregistre le résultat au format json dans un fichier au lieu de l'afficher dans la console.
Le nom du fichier est output.json
'''
nbVals = 40
time_limit = 5
start = time.time()
vals = []
OUTPUT_JSON = "output/output.json"

# On instancie un CustomWriterJSON
writer = CustomWriterJSON(target_file=OUTPUT_JSON)
fulljson = []

# Tant que le temps n'est pas écoulé
while time.time() - start < time_limit:
    # On récupère la réponse du simulateur, sous forme de tuple (date, valeur)
    response = arduino_simulator.simulateReadAnalogPin()
    vals.append(response[1])
    if len(vals) == nbVals:
        mean = MathHelper.getMean(vals)
        mediane = MathHelper.getMediane(vals)
        fulljson.append({"Date": response[0], "Moyenne": mean, "Mediane": mediane})
        vals.clear()

writer.write_json(fulljson, on_error=lambda: print("An error ocurred writing file."),
                  on_success=lambda: print("Successfully wrote ", OUTPUT_JSON))
