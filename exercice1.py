# -*- coding: utf-8 -*-

import time
import sys

from helpers import MathHelper, arduino_simulator
from writer.CustomWriterCSV import CustomWriterCSV

''' 
Exercice 1: 3 points

Ecrire un programme qui reprend exercice_exemple.py mais qui fait un affichage dans la console au format csv.

Voici les informations à afficher dans chaque ligne:
- la valeur renoyée par simulateReadAnalogPin
- La date d'appel de la fonction simulateReadAnalogPin

La première ligne affiche les titres des colonnes qui sont dans l'ordre:
- moyenne
- date

Le programme tounre environ 5 secondes.
'''

nbVals = 10
time_limit = 5
start = time.time()
vals = []

# On instancie un CustomCsvWriter
writer = CustomWriterCSV(fieldnames=["Date", "Moyenne"], delimiter="|", targetfile=sys.stdout)
# On écrit le header
writer.writeheader()

# Tant que le temps n'est pas écoulé
while time.time() - start < time_limit:
    # On récupère la réponse du simulateur, sous forme de tuple (date, valeur)
    response = arduino_simulator.simulateReadAnalogPin()
    vals.append(response[1])
    if len(vals) == nbVals:
        mean = MathHelper.getMean(vals)
        writer.writerow({"Date": response[0], "Moyenne": mean})
        vals.clear()
